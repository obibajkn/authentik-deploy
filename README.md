# Deploying Traefik using forward proxy mode with Authentik

This is an example guide how to deploy Authentik with Traefik in forward auth proxy mode - that means that any application behind the
proxy will be automatically authenticated by Traefik. This allows better reuse of code and completely moves user management to Traefik &
Authentik.

In this guide we use custom DNS to make the requests nicer and to show that it works with DNS. So step #1 is to put following records to
your `/etc/hosts` (for example by `sudo nano /etc/hosts` and adding these values)

```bash
# domains needed for traefik & authentik example
127.0.0.1 app.example.com
127.0.0.1 auth.example.com
```

Now we need to start the applications - it is not completely automated, even thought we set the dependencies, Traefik does not pick the
middleware from the beginning, so it's better to start it one by one. Also check the logs per app if it started.
First make sure you have correct `docker-compose.yml` file in the current folder (part of this gist).

1. start databases -> `docker compose up -d postgresql redis`
2. start worker & authentik server -> `docker compose up -d worker server`
3. now go to [http://localhost:9000/if/flow/initial-setup/](http://localhost:9000/if/flow/initial-setup/) and create new account for
   authentik (the username is `akadmin`)
4. setup application [http://localhost:9000/if/admin/#/core/applications](http://localhost:9000/if/admin/#/core/applications) including
   provider
5. when setting up provider, choose `Forward auth (single application)` option
6. setup outpost [http://localhost:9000/if/admin/#/outpost/outposts](http://localhost:9000/if/admin/#/outpost/outposts) that points to
   the application
7. click on View Deployment Info and copy token -> this token then put to the `docker-compose.yml` to `authentik-proxy` service as
   environment variable `AUTHENTIK_TOKEN=<copied data>`
8. start proxy -> `docker compose up -d authentik-proxy`
9. start our real app -> `docker compose up -d whoami`
10. if logs look good (especially `authentik-proxy` needs to be checked), start Traefik -> `docker compose up -d traefik`
11. now everything should be up & running -> app is accessible from [app.example.com](http://app.example.com) and authentik admin 
    interface from [auth.example.com](http://auth.example.com) 
12. go to app and you will be asked to log in, use credentials created in step #3
